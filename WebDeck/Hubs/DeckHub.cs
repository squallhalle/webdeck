﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebDeck.Models;
using WebDeck.Services;

namespace WebDeck.Hubs
{
    public class DeckHub : Hub
    {
        private readonly OBSService oBSService;
        private readonly HueService hueService;
        private readonly SpotifyService spotifyService;
        private readonly BlacklightService blacklightService;
        private readonly MonsterCatService monsterCatService;
        private readonly IWebHostEnvironment hostEnvironment;
        private readonly ILogger<DeckHub> logger;

        public DeckHub(OBSService oBSService,
            HueService hueService,
            SpotifyService spotifyService,
            BlacklightService blacklightService,
            MonsterCatService monsterCatService,
            IWebHostEnvironment hostEnvironment,
            ILogger<DeckHub> logger)
        {
            this.oBSService = oBSService;
            this.hueService = hueService;
            this.spotifyService = spotifyService;
            this.blacklightService = blacklightService;
            this.monsterCatService = monsterCatService;
            this.hostEnvironment = hostEnvironment;
            this.logger = logger;
        }
        public async Task SendDeckCommand(string val)
        {
            //await Clients.All.SendAsync("ReceiveMessage", user, message);
            if (!String.IsNullOrEmpty(val))
            {
                logger.LogInformation($"Post with: {val}");
                List<string> vallist = val.Split("#").ToList();
                if (vallist.Count == 2)
                {
                    string commandtype = vallist[0];
                    string commandtext = vallist[1];
                    switch (commandtype)
                    {
                        case "MonsterCat":
                            await monsterCatService.RunMonsterStuff();
                            break;
                        case "OBS":
                            await oBSService.HandleOBSCommand(commandtext);
                            break;
                        case "HUE":
                            await hueService.HandleHueCommand(commandtext);
                            break;
                        case "BLACKLIGHT":
                            await blacklightService.HandleBlacklightCommand(commandtext);
                            break;
                        case "SPOTIFY":
                            if (spotifyService.client != null)
                            {
                                await spotifyService.HandleSpotifyCommand(commandtext);
                                break;
                            }
                            else
                            {
                                string loginrequest = null;
                                if (this.hostEnvironment.IsDevelopment())
                                {
                                    loginrequest = await spotifyService.GetLoginRequest("http://localhost:5000/Admin?handler=SpotifyReturnDeck");

                                }
                                else
                                {
                                    loginrequest = await spotifyService.GetLoginRequest("http://bullshit.here:2000/Admin?handler=SpotifyReturnDeck");
                                }
                                await Clients.Caller.SendAsync("SpotifyRedirect", loginrequest);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

    }
}

﻿"use strict";

var connection = new signalR.HubConnectionBuilder()
    .withAutomaticReconnect()
    .configureLogging(signalR.LogLevel.Information)
    .withUrl("/DeckHub")
    .build();

//Disable send button until connection is established

async function start() {
    try {
        await connection.start();
        console.log("connected");
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};

connection.onclose(async () => {
    await start();
});



connection.on("SpotifyRedirect", function (url) {
    console.log(url);
    window.open(url, "_self")
});




function sendDeckCommand(objButton) {
    console.log(objButton.value);
    connection.invoke("SendDeckCommand", objButton.value).catch(function (err) {
        return console.error(err.toString());
    });


    // Start the connection.
    start();
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebDeck.Models;
using WebDeck.Models.SQL.MonsterCatDB;

namespace WebDeck.Services
{
    public class MonsterCatService
    {
        HttpClient httpClient = new HttpClient();
        string url = "https://connect.monstercat.com/";
        MonserDBContext context;
        private readonly ILogger<MonsterCatService> logger;

        public MonsterCatService(ILogger<MonsterCatService> logger)
        {
            httpClient.BaseAddress = new Uri(url);
            this.logger = logger;
        }

        public async Task RunMonsterStuff()
        {
            try
            {
                logger.LogInformation($"Begin scriping...");
                List<Result> ResultList = new List<Result>();

                for (int i = 0; i <= 2050; i = i + 25)
                {
                    var response = await GetMonsterResponse(i);
                    if (response != null && response.Results.Count > 0)
                    {
                        ResultList.AddRange(response.Results);
                    }
                }
                int changes = await SaveToDB(ResultList);
                logger.LogInformation($"Done with {changes} changes");
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception on: RunMonsterStuff:{ex.Message}", ex);
            }
        }

        public async Task<MonsterRespone> GetMonsterResponse(int skip)
        {
            try
            {
                string responestring = await httpClient.GetStringAsync($"v2/catalog/browse?creatorfriendly=1&limit=25&skip={skip}");
                if (!string.IsNullOrEmpty(responestring))
                {
                    var monsterRespone = MonsterRespone.FromJson(responestring);
                    if (monsterRespone != null)
                    {
                        logger.LogInformation($"Asked for skipped:{skip}, result:{monsterRespone.Results.Count}");
                        return monsterRespone;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on GetMonsterResponse: {ex.Message}", ex);
            }
            return null;
        }

        public async Task<List<Result>> GetResultsFromDatabase()
        {
            List<Result> returnlist = null;
            try
            {
                using (context = new MonserDBContext())
                {
                    returnlist = context.Results
                        .Include(i => i.resultArtists)
                        .ThenInclude(i => i.artist)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on GetResultsFromDatabase:{ex.Message}", ex);
            }
            return returnlist;
        }

        /// <summary>
        /// MONSTER ! at FutureME: Sorry for that 
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        public async Task<int> SaveToDB(List<Result> results)
        {
            try
            {
                using (context = new MonserDBContext())
                {
                    foreach (var result in results)
                    {
                        Result tempresult = context.Results.FirstOrDefault(x => x.Id == result.Id);
                        if (tempresult == null)
                        {
                            List<Artist> newlist = new List<Artist>();
                            Release temprelase = null;
                            foreach (var artist in result.Artists)
                            {
                                Artist tempArt = context.Artist.FirstOrDefault(x => x.Id == artist.Id);

                                if (tempArt == null)
                                {
                                    context.Artist.Add(artist);
                                    newlist.Add(artist);
                                    tempArt = artist;
                                    await context.SaveChangesAsync();
                                }
                                else
                                {
                                    newlist.Add(tempArt);
                                }
                            }
                            temprelase = context.Release.FirstOrDefault(x => x.Id == result.Release.Id);
                            if (temprelase == null)
                            {
                                context.Release.Add(result.Release);
                                temprelase = result.Release;
                                await context.SaveChangesAsync();
                            }
                            result.Release = temprelase;
                            result.Artists = newlist;
                            tempresult = result;
                            context.Results.Add(result);
                            await context.SaveChangesAsync();
                        }
                        else
                        {
                            if (tempresult.CreatorFriendly != result.CreatorFriendly)
                            {
                                tempresult.CreatorFriendly = result.CreatorFriendly;
                                await context.SaveChangesAsync();
                            }
                        }
                        foreach (var artist in result.Artists)
                        {
                            var tempresultart = await context.ResultArtist.FirstOrDefaultAsync(x => x.ArtistId == artist.Id && x.ResultId == result.Id);
                            if (tempresultart == null)
                            {
                                await context.ResultArtist.AddAsync(new ResultArtist() { ArtistId = artist.Id, ResultId = result.Id });
                                await context.SaveChangesAsync();
                            }
                        }
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {

                if (ex.InnerException != null)
                {
                    Console.WriteLine($"Error on SaveToDB: {ex.Message} ; {ex.InnerException.Message}");
                }
                else
                {
                    Console.WriteLine($"Error on SaveToDB: {ex.Message}");
                }
            }
            return -666;
        }

    }
}

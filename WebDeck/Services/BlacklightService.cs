﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebDeck.Services
{
    public class BlacklightService
    {
        private readonly IHttpClientFactory httpClientFactory;

        public BlacklightService(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public async Task HandleBlacklightCommand(string command)
        {
            var client = httpClientFactory.CreateClient("Blacklight");
            switch (command)
            {
                case "ON":
                    var resulton = await client.GetStringAsync("/gpio/0");
                    break;
                case "OFF":
                    var resultoff = await client.GetStringAsync("/gpio/1");
                    break;
                default:
                    break;
            }
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using OBS.WebSocket.NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebDeck.Models;

namespace WebDeck.Services
{
    public class OBSService
    {
        private readonly ObsWebSocket obsWebSocket;
        private readonly ILogger<OBSService> logger;
        private readonly ConfigModel config;

        public OBSService(ObsWebSocket obsWebSocket, ILogger<OBSService> logger, ConfigModel config)
        {
            this.obsWebSocket = obsWebSocket;
            this.logger = logger;
            this.config = config;
        }

        public async Task<bool> IsConnected()
        {
            return obsWebSocket.IsConnected;
        }
        public async Task HandleOBSCommand(string command)
        {
            if (!obsWebSocket.IsConnected)
            {
                obsWebSocket.Connect(config.oBSConfig.Connectionstring, config.oBSConfig.Password);

            }

            if (!String.IsNullOrEmpty(command) && obsWebSocket.IsConnected)
            {
                List<string> vallist = command.Split("!").ToList();
                if (vallist.Count == 2)
                {
                    string obscommandtype = vallist[0];
                    string obscommandtext = vallist[1];
                    try
                    {
                        switch (obscommandtype)
                        {
                            case "Scene":
                                obsWebSocket.Api.SetCurrentScene(obscommandtext);
                                break;
                            case "CAST":
                                if (obscommandtext == "start")
                                {
                                    obsWebSocket.Api.StartStreaming();

                                }
                                else
                                {
                                    obsWebSocket.Api.StopStreaming();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        logger.LogInformation("Ha! OBS Command was not succesful");

                    }
                }
            }
        }


    }
}

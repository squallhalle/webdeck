﻿using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using OBS.WebSocket.NET;
using Q42.HueApi;
using Q42.HueApi.Interfaces;
using Q42.HueApi.Models.Groups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebDeck.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using Q42.HueApi.Models;

namespace WebDeck.Models
{
    public class HueService
    {

        private readonly ILogger<HueService> logger;
        private readonly ConfigModel config;
        ILocalHueClient client;
        public HueService(ILogger<HueService> logger, ConfigModel config)
        {
            this.logger = logger;
            this.config = config;
            client = new LocalHueClient(config.hueConfig.IP);
            client.Initialize(config.hueConfig.Key);
        }

        public async Task HandleHueCommand(string command)
        {
            switch (command)
            {
                case "on":
                    await SetOn();
                    break;
                case "off":
                    await SetOff();
                    break;
                case "neo":
                    await SetTokyo();
                    break;
                case "colorful":
                    await SetColorful();
                    break;
                case "colorloop":
                    await SetColorloop();
                    break;
                default:
                    break;
            }
        }

        private async Task SetOn()
        {
            var command = new LightCommand();
            command.TurnOn();
            var fuu = await client.GetBridgeAsync();
            await client.SendCommandAsync(command);
        }

        private async Task SetOff()
        {
            var command = new LightCommand();
            command.TurnOff();
            try
            {
                await client.SendCommandAsync(command);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on Hue SetOff: {ex.Message}", ex);
            }
        }

        private async Task<string> GetSecene(string name)
        {
            try
            {
                List<Scene> scenes = (await client.GetScenesAsync()).ToList();
                Scene scene = scenes.FirstOrDefault(x => x.Name == name);
                if (scene != null)
                {
                    return scene.Id;
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on Hue GetSecene: {ex.Message}", ex);
            }
            return string.Empty;
        }

        private async Task SetTokyo()
        {
            try
            {
                string id = await GetSecene("neo");
                if (!string.IsNullOrEmpty(id))
                {
                    SceneCommand sceneCommand = new SceneCommand() { Scene = "PsYt3adZ8eSqTyc" };
                    LightCommand lightCommand = new LightCommand() { Brightness = 254 };
                    await client.SendGroupCommandAsync(sceneCommand, "1");
                    await client.SendGroupCommandAsync(lightCommand, "1");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on Hue SetTokyo: {ex.Message}", ex);
            }
        }
        private async Task SetColorful()
        {
            try
            {
                string id = await GetSecene("colorful");
                if (!string.IsNullOrEmpty(id))
                {
                    SceneCommand sceneCommand = new SceneCommand() { Scene = id };
                    await client.SendGroupCommandAsync(sceneCommand, "1");
                    LightCommand lightCommand = new LightCommand() { Brightness = 254 };
                    await client.SendGroupCommandAsync(lightCommand, "1");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on Hue SetColorful: {ex.Message}", ex);
            }
        }
        private async Task SetColorloop()
        {
            try
            {
                SensorState sensorState = new SensorState() { Status = 1 };
                await client.ChangeSensorStateAsync("31", sensorState);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on Hue SetColorloop: {ex.Message}", ex);
            }
        }
    }
}

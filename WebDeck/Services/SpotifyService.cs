﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.FlowAnalysis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SpotifyAPI.Web;
using WebDeck.Models;
using WebDeck.Pages;
using Microsoft.AspNetCore.Mvc;
using static SpotifyAPI.Web.PlaylistRemoveItemsRequest;

namespace WebDeck.Services
{
    public class SpotifyService
    {
        private readonly ILogger<SpotifyService> logger;
        private readonly ConfigModel config;
        private readonly MonsterCatService monsterCatService;
        public SpotifyClient client;

        public SpotifyService(ILogger<SpotifyService> logger, ConfigModel config, MonsterCatService monsterCatService)
        {
            this.logger = logger;
            this.config = config;
            this.monsterCatService = monsterCatService;
        }

        public async Task SetAcessToken(string token)
        {

            var config = SpotifyClientConfig
                .CreateDefault(token)
                .WithRetryHandler(new SimpleRetryHandler() { TooManyRequestsConsumesARetry = true, RetryAfter = TimeSpan.FromSeconds(5) });
            client = new SpotifyClient(config);
        }

        public async Task HandleSpotifyCommand(string command)
        {
            if (client != null)
            {
                if (!String.IsNullOrEmpty(command))
                {
                    List<string> vallist = command.Split("!").ToList();
                    if (vallist.Count == 2)
                    {
                        string obscommandtype = vallist[0];
                        string obscommandtext = vallist[1];
                        try
                        {
                            switch (obscommandtype)
                            {
                                case "Controle":
                                    switch (obscommandtext)
                                    {
                                        case "Play":
                                            var current = await client.Player.GetCurrentPlayback();
                                            if (current != null)
                                            {
                                                if (current.IsPlaying)
                                                {
                                                    await client.Player.PausePlayback();
                                                }
                                                else
                                                {
                                                    await client.Player.ResumePlayback();
                                                }
                                            }
                                            break;
                                        case "Previous":
                                            await client.Player.SkipPrevious();
                                            break;
                                        case "Next":
                                            await client.Player.SkipNext();
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                case "Playlist":
                                    switch (obscommandtext)
                                    {
                                        case "updateMonsterCat":
                                            await UpdateMonsterCatPlaylist();
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError($"HaHAha! Sporitfy go BRRRRRR: {ex.Message}", ex);
                        }
                    }
                }
            }
        }

        public async Task UpdateMonsterCatPlaylist()
        {
            logger.LogInformation("Start updating MonstercatPlaylist");
            logger.LogInformation("GetPlaylistID");
            string WebDeckPlaylistID = await GetWebDeckPlaylistID();
            logger.LogInformation("Get Items to Remove");
            List<string> ItemsToRemove = await GetPlaylistRemoveItems(WebDeckPlaylistID);
            logger.LogInformation("RemoveThem");
            await RemoveItemsFromPlaylist(WebDeckPlaylistID, ItemsToRemove);
            logger.LogInformation("Get new songs from DB");
            List<Result> newsongsfromdb = await this.monsterCatService.GetResultsFromDatabase();
            logger.LogInformation("Find new songs on Spotify");
            List<FullTrack> newsongsfromspotify = await FindSpotifySongs(newsongsfromdb);
            logger.LogInformation("add them to the playlist");
            await AddSongsToPlaylist(WebDeckPlaylistID, newsongsfromspotify);
            logger.LogInformation("Done");
        }

        private async Task AddSongsToPlaylist(string WebDeckPlaylistID, List<FullTrack> newsongsfromspotify)
        {
            try
            {
                int amount = 100;
                List<string> urilist = newsongsfromspotify.Select(x => x.Uri).ToList();
                while (urilist.Count > 0)
                {
                    try
                    {
                        if (urilist.Count < 100)
                        {
                            amount = urilist.Count;
                        }
                        var templist = urilist.Take(amount).ToList();
                        PlaylistAddItemsRequest request = new PlaylistAddItemsRequest(templist);
                        await client.Playlists.AddItems(WebDeckPlaylistID, request);
                        urilist.RemoveRange(0, amount);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError($"Error on AddSongsToPlaylist-WhileLoop :{ex.Message}", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on AddSongsToPlaylist :{ex.Message}", ex);
            }

        }
       
        private async Task<List<FullTrack>> FindSpotifySongs(List<Result> newsongsfromdb)
        {
            List<FullTrack> returnList = new List<FullTrack>();
            foreach (var result in newsongsfromdb)
            {
                string artistname = "";
                try
                {
                    var artist = (result.resultArtists.FirstOrDefault(x => x.artist.Role == "Primary"));
                    if (artist != null)
                    {
                        artistname = artist.artist.Name;
                    }
                    else
                    {
                        logger.LogWarning($"{result.Title} has not Primay Artist .. skip");
                    }
                }
                catch
                {
                    string br = "";
                    //artistname = (result.resultArtists.FirstOrDefault()).artist.Name;
                }
                string titel = result.Title;
                if (!string.IsNullOrEmpty(artistname))
                {
                    var search = await client.Search.Item(new SearchRequest(SearchRequest.Types.Track, $"{artistname} {titel}"));
                    if (search.Tracks != null && search.Tracks.Items.Count > 0)
                    {
                        var track = search.Tracks.Items.OrderByDescending(o => o.Popularity).FirstOrDefault();
                        logger.LogTrace($"Seached for:{artistname},{titel} => {track.Artists.FirstOrDefault().Name},{track.Name}");
                        List<string> spotifyartistsnames = track.Artists.Select(x => x.Name.ToLower()).ToList();
                        if (!spotifyartistsnames.Contains(artistname.ToLower()))
                        {
                            logger.LogWarning($"Different Artist ? Seached for:{artistname.ToLower()},{titel} => {String.Join(',', spotifyartistsnames)} ; {track.Name}");
                        }
                        else
                        {
                            if (!returnList.Any(x => x.Uri == track.Uri))
                            {
                                returnList.Add(track);
                            }
                            else
                            {
                                logger.LogWarning($"already in list:{String.Join(',', spotifyartistsnames)} ; {track.Name}");
                            }
                        }

                    }
                }
            }
            return returnList;
        }

        private async Task<string> GetWebDeckPlaylistID()
        {
            try
            {
                var userid = await client.UserProfile.Current();
                var allplaylistpage = await client.Playlists.GetUsers(userid.Id);
                var allplaylists = await client.PaginateAll(allplaylistpage);
                var webdeckplaylist = allplaylists.FirstOrDefault(x => x.Name == "Monstercat Gold (all CreatorFriendly Songs)");
                return webdeckplaylist.Id;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error on GetWebDeckPlaylistID: {ex.Message}", ex);
            }
            return string.Empty;
        }

        private async Task<List<string>> GetPlaylistRemoveItems(string WebDeckPlaylistID)
        {
            List<string> urilist = new List<string>();
            try
            {

                var allplaylistsongspage = await client.Playlists.GetItems(WebDeckPlaylistID);
                var allplaylistsongs = (await client.PaginateAll(allplaylistsongspage)).ToList();
                urilist = allplaylistsongs.Select(x => x.Track).Cast<FullTrack>().Select(x => x.Uri).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error on GetPlaylistRemoveItems {ex.Message}");
            }
            return urilist;

        }

        private async Task RemoveItemsFromPlaylist(string WebDeckPlaylistID, List<string> ItemsToRemove)
        {
            while (ItemsToRemove.Count > 0)
            {
                try
                {
                    int removed = 0;
                    int amount = 100;
                    if (ItemsToRemove.Count < 100)
                    {
                        amount = ItemsToRemove.Count;
                    }
                    List<string> Templist = ItemsToRemove.Take(amount).ToList();
                    try
                    {
                        var list = Templist.Select(x => new Item() { Uri = x }).ToList();
                        await client.Playlists.RemoveItems(WebDeckPlaylistID, new PlaylistRemoveItemsRequest()
                        {
                            Tracks = list
                        });
                        removed = removed + amount;
                        logger.LogInformation($"REMOVED:{removed}");
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"RemoveItemsFromPlaylist GOOOO BRRRRR: {ex.Message}");
                    }
                    ItemsToRemove.RemoveRange(0, amount);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"RemoveItemsFromPlaylist-WhileLoop GOOOO BRRRRR: {ex.Message}");
                }
            }

        }

        public async Task<string> GetLoginRequest(string rediecturl)
        {
            var loginRequest = new LoginRequest(new Uri(rediecturl), config.spotifyConfig.ClientID, LoginRequest.ResponseType.Code)
            {
                Scope = new[] { Scopes.PlaylistReadPrivate
                , Scopes.PlaylistReadCollaborative
                , Scopes.AppRemoteControl
                , Scopes.UserModifyPlaybackState
                , Scopes.UserReadCurrentlyPlaying
                , Scopes.UserReadPlaybackPosition
                ,Scopes.Streaming
                ,Scopes.UserReadPlaybackState
                ,Scopes.PlaylistModifyPrivate,
                Scopes.PlaylistModifyPublic
                }
            };
            return loginRequest.ToUri().OriginalString;
        }

        public async Task HandleLoginRespone(string code, string redirectrl)
        {
            var response = await new OAuthClient().RequestToken(new AuthorizationCodeTokenRequest(config.spotifyConfig.ClientID, config.spotifyConfig.ClientSecret, code, new Uri(redirectrl)));
            await this.SetAcessToken(response.AccessToken);
        }
    }
}

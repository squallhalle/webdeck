﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
//using Unosquare.Swan;
using WebDeck.Models;
using WebDeck.Models.SQL;

namespace WebDeck.Pages
{
    [Authorize]

    public class IconConfigurationModel : PageModel
    {
        private readonly IServiceScopeFactory serviceScopeFactory;
        private readonly ConfigModel configModel;
        public List<IconConfig> iconConfigs;
        public List<string> UsedProperties;
        public string teststring = "hugo";
        [BindProperty]  public string hugo2 { get; set; }

        public IconConfigurationModel(IServiceScopeFactory serviceScopeFactory, ConfigModel configModel  )
        {
            this.serviceScopeFactory = serviceScopeFactory;
            this.configModel = configModel;
            this.UsedProperties = new List<string>() { "IconPath", "CommandType", "CommandText", "PosX", "PosY", "FkPage" };
        }
        public async Task OnGet()
        {
            await GetIconConfig();
        }
        
        //TODO: test 
        private async Task GetIconConfig()
        {
            using (var scope = serviceScopeFactory.CreateScope())
            {

                try
                {
                    using (var _dbcontext = scope.ServiceProvider.GetRequiredService<IconConfigContext>())
                    {
                        iconConfigs = _dbcontext.IconConfig.Where(x => x.FkPage == 1).ToList();
                    }
                }
                catch (Exception ex)
                {
                    string br = "";
                    throw;
                }
            }
        }

        private async Task OnPostAsnyc()
        {
            var newstring = teststring;
            await GetIconConfig();
        }

        private void OnPost()
        {
          //  GetIconConfig().Await();
        }

        //private async Task OnPostHugoAsync()
        //{
        //    bool saveit = true;
        //    await GetIconConfig();
        //    string br = "";

        //}

        //public void OnPostPeter(IconConfigurationModel newiconConfigs)
        //{
        //    var temp = iconConfigs.Count;
        //    GetIconConfig().Await();
        //}
        public void OnPostPeter()
        {
            var temp = iconConfigs.Count;
           // GetIconConfig().Await();
        }

        public void OnPostHugo()
        {
             var temp = iconConfigs.Count;
          //GetIconConfig().Await();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Server.IIS.Core;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using OBS.WebSocket.NET;
using OBS.WebSocket.NET.Types;
using WebDeck.Models;
using WebDeck.Models.SQL;
using System.IO.Pipes;
using System.Net.Http;
using WebDeck.Services;

namespace WebDeck.Pages
{
    public class DeckModel : PageModel
    {
        private ObsWebSocket ObsWebSocket;
        private readonly HueService hueService;
        private List<OBSScene> SceneList;
        private readonly ILogger<DeckModel> _logger;
        private readonly DumbAssIconListForLazyDevs dumbAssIcon;
        public List<IconConfig> iconPage;
        private Random rnd = new Random();
        private readonly IconConfigContext _iconConfigContext;
        private readonly ConfigModel config;
        private readonly OBSService oBSService;
        private readonly SpotifyService spotifyService;
        private readonly BlacklightService blacklightService;

        public DeckModel(ObsWebSocket obsWebSocket, HueService hueService, ILogger<DeckModel> logger, DumbAssIconListForLazyDevs dumbAssIcon, IconConfigContext iconConfigContext, ConfigModel config, 
                OBSService oBSService, SpotifyService spotifyService, BlacklightService blacklightService)
        {
            this.ObsWebSocket = obsWebSocket;
            this.hueService = hueService;
            this._logger = logger;
            this._iconConfigContext = iconConfigContext;
            this.config = config;
            this.oBSService = oBSService;
            this.spotifyService = spotifyService;
            this.blacklightService = blacklightService;
            this.dumbAssIcon = dumbAssIcon;
            this.iconPage = new List<IconConfig>();
        }
        public async Task OnGet()
        {
            await DoStuff();
        }

        private async Task DoStuff()
        {
            using (this._iconConfigContext)
            {
                this.iconPage = _iconConfigContext.IconConfig.ToList();
            }
            //TODO: move "checkDisabled" into an own methode for each serivce 
            if (spotifyService.client == null)
            {
                iconPage.Where(x => x.CommandType == "SPOTIFY").ToList().ForEach(x => x.Disabled = true);
            }
            if ((await oBSService.IsConnected()) == false)
            {
                iconPage.Where(x => x.CommandType == "OBS").ToList().ForEach(x => x.Disabled = true);
            }
        }

    }
}
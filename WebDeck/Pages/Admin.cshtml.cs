﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Primitives;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Controller;
using OBS.WebSocket.NET;
using SpotifyAPI.Web;
using WebDeck.Models;
using WebDeck.Services;

namespace WebDeck.Pages
{
    [Authorize]
    public class AdminModel : PageModel
    {
        public ConfigModel config;
        public ObsWebSocket obsWebSocket;
        public readonly SpotifyService spotifyService;
        private readonly IWebHostEnvironment env;

        public AdminModel(ConfigModel config, ObsWebSocket obsWebSocket, SpotifyService spotifyService, IWebHostEnvironment env)
        {
            this.config = config;
            this.obsWebSocket = obsWebSocket;
            this.spotifyService = spotifyService;
            this.env = env;
        }



        public void OnGet()
        {
            string br = "";
        }

        public async Task<IActionResult> OnPostSpotify()
        {
            string requesturl = null;
            string thisurl = null;
            if (this.env.IsDevelopment())
            {
                thisurl = "http://localhost:5000/Admin?handler=SpotifyReturn";
            }
            else
            {
                thisurl = "http://bullshit.here:2000/Admin?handler=SpotifyReturn";
            }
            requesturl = await spotifyService.GetLoginRequest(thisurl);
            return Redirect(requesturl);
        }

        public async Task<IActionResult> OnGetSpotifyReturn(string code)
        {
            string requesturl = null;
            if (this.env.IsDevelopment())
            {
                requesturl = "http://localhost:5000/Admin?handler=SpotifyReturn";
            }
            else
            {
                requesturl = "http://bullshit.here:2000/Admin?handler=SpotifyReturn";
            }
            await spotifyService.HandleLoginRespone(code, requesturl);
            return Redirect("/Admin");
        }
        public async Task<IActionResult> OnGetSpotifyReturnDeck(string code)
        {
            string requesturl = null;
            if (this.env.IsDevelopment())
            {
                requesturl = "http://localhost:5000/Admin?handler=SpotifyReturnDeck";
            }
            else
            {
                requesturl = "http://bullshit.here:2000/Admin?handler=SpotifyReturnDeck";

            }
            await spotifyService.HandleLoginRespone(code, requesturl);
            return Redirect("/Deck");

        }

        public async Task<IActionResult> OnGetWay2Async(string data)
        {

            return StatusCode(200);
        }

        public void OnPost()
        {
            if (obsWebSocket.IsConnected)
            {
                obsWebSocket.Disconnect();
            }
            else
            {
                try
                {
                    obsWebSocket.Connect(config.oBSConfig.Connectionstring, config.oBSConfig.Password);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"OBS could not connect: {ex.InnerException}");
                }
            }
        }
    }
}
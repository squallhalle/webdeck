﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebDeck.Models
{
    public class DumbAssIconListForLazyDevs
    {
        public List<string> IconList { get; set; }

        public DumbAssIconListForLazyDevs()
        {
            this.IconList = new List<string>()
            {
                "fa-cat",
                "fa-ghost",
                "fa-crow",
                "fa-skull-crossbones",
                "fa-toilet-paper",
                "fa-battery-full",
                "fa-dragon",
                "fa-birthday-cake"
            };

        }
    }
}

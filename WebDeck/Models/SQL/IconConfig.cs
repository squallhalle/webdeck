﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebDeck.Models.SQL
{
    public partial class IconConfig
    {
        public int Id { get; set; }
        public string IconPath { get; set; }
        public string CommandType { get; set; }
        public string CommandText { get; set; }
        public string DisplayName { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public int FkPage { get; set; }
        [NotMapped]
        public bool Disabled { get; set; }
        public virtual IconPage FkPageNavigation { get; set; }

        public IconConfig()
        {
            Disabled = false;
        }
    }

}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebDeck.Models.SQL
{
    public partial class IconConfigContext : DbContext
    {
        //public IconConfigContext()
        //{
        //}

        public IconConfigContext(DbContextOptions<IconConfigContext> options)
            : base(options)
        {
        }

        public virtual DbSet<IconConfig> IconConfig { get; set; }
        public virtual DbSet<IconPage> IconPage { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP\\TESTDB;Database=IconConfig;Trusted_Connection=True;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IconConfig>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CommandText)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CommandType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FkPage).HasColumnName("FK_Page");

                entity.Property(e => e.IconPath)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.FkPageNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.FkPage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TempSales_SalesReason");
            });

            modelBuilder.Entity<IconPage>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PageName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebDeck.Models.SQL.MonsterCatDB
{
    class MonserDBContext : DbContext
    {

        public virtual DbSet<Artist> Artist { get; set; }
        public virtual DbSet<Result> Results { get; set; }
        public virtual DbSet<Release> Release { get; set; }
        public virtual DbSet<ResultArtist> ResultArtist { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Artist>(entity =>
            {
                entity.HasKey(c=>c.Id);
                //entity.HasMany(c => c.Results);
            });
            builder.Entity<Result>(entity =>
            {
                entity.HasKey(e=>e.Id);
                //entity.HasMany(e => e.Artists);
                entity.Ignore("Artists");
                entity.Ignore(e=>e.Release);
            });
            builder.Entity<Release>(entity =>
            {
                entity.HasKey(e=>e.Id);
            });
            
            builder.Entity<ResultArtist>()
            .HasKey(bc => new { bc.ResultId, bc.ArtistId });
            builder.Entity<ResultArtist>()
                .HasOne(bc => bc.result)
                .WithMany(b => b.resultArtists)
                .HasForeignKey(bc => bc.ResultId);
            builder.Entity<ResultArtist>()
                .HasOne(bc => bc.artist)
                .WithMany(c => c.resultArtists)
                .HasForeignKey(bc => bc.ArtistId);

            //builder.Entity<ResultArtist>().HasKey(sc => new { sc.ResultId, sc.ArtistId });
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Monster.db").EnableDetailedErrors().EnableSensitiveDataLogging();
        }

    }
}

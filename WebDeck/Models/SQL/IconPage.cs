﻿using System;
using System.Collections.Generic;

namespace WebDeck.Models.SQL
{
    public partial class IconPage
    {
        public int Id { get; set; }
        public string PageName { get; set; }
    }
}

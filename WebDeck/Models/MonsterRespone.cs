﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebDeck.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class MonsterRespone
    {
        [JsonProperty("limit")]
        public long Limit { get; set; }

        [JsonProperty("notFound")]
        public string NotFound { get; set; }

        [JsonProperty("results")]
        public List<Result> Results { get; set; }

        [JsonProperty("skip")]
        public long Skip { get; set; }

        [JsonProperty("total")]
        public long Total { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("artists")]
        public List<Artist> Artists { get; set; }

        [JsonProperty("artistsTitle")]
        public string ArtistsTitle { get; set; }

        [JsonProperty("bpm")]
        public long Bpm { get; set; }

        [JsonProperty("creatorFriendly")]
        public bool CreatorFriendly { get; set; }

        [JsonProperty("debutDate")]
        public DateTimeOffset DebutDate { get; set; }

        [JsonProperty("downloadable")]
        public bool Downloadable { get; set; }

        [JsonProperty("dution")]
        public long Duration { get; set; }

        [JsonProperty("explicit")]
        public bool Explicit { get; set; }

        [JsonProperty("genrePrimary")]
        public string GenrePrimary { get; set; }

        [JsonProperty("genreSecondary")]
        public string GenreSecondary { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("inEarlyAccess")]
        public bool InEarlyAccess { get; set; }

        [JsonProperty("isrc")]
        public string Isrc { get; set; }

        [JsonProperty("playlistSort")]
        public long PlaylistSort { get; set; }

        [JsonProperty("release")]
        public Release Release { get; set; }

        [JsonProperty("streamable")]
        public bool Streamable { get; set; }

        //[JsonProperty("tags")]
        //public List<string> Tags { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("trackNumber")]
        public long TrackNumber { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }


        public ICollection<ResultArtist>  resultArtists { get; set; }
    }

    public class ResultArtist
    {
        public Guid ResultId { get; set; }
        public Result result { get; set; }
        public Guid ArtistId { get; set; }
        public Artist artist { get; set; }
    }

    public partial class Artist
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("public")]
        public bool Public { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        //public IList<Result> Results { get; set; }
        public ICollection<ResultArtist> resultArtists { get; set; }
    }

    public partial class Release
    {
        [JsonProperty("artistsTitle")]
        public string ArtistsTitle { get; set; }

        [JsonProperty("catalogId")]
        public string CatalogId { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("releaseDate")]
        public DateTimeOffset ReleaseDate { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("upc")]
        public string Upc { get; set; }
    }

    public partial class MonsterRespone
    {
        public static MonsterRespone FromJson(string json) => JsonConvert.DeserializeObject<MonsterRespone>(json, WebDeck.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this MonsterRespone self) => JsonConvert.SerializeObject(self, WebDeck.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace WebDeck.Models
{
    public class ConfigModel
    {
        private readonly IWebHostEnvironment env;
        public ConfigModel(IWebHostEnvironment env)
        {
            this.env = env;
        }
        public OBSConfig oBSConfig { get; set; }
        public HueConfig hueConfig { get; set; }

        public SpotifyConfig spotifyConfig { get; set; }

        public BlacklightConfig blacklightConfig { get; set; }

        public void Save()
        {
            try
            {
                System.IO.File.WriteAllText(env.ContentRootPath + @"/Config/Config.json", JsonConvert.SerializeObject(this));
            }
            catch (Exception ex)
            {

            }
        }

        public static ConfigModel Load(IWebHostEnvironment env)
        {
            try
            {
                return JsonConvert.DeserializeObject<ConfigModel>(System.IO.File.ReadAllText(env.ContentRootPath + @"/Config/Config.json"));
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }

    public class HueConfig
    {
        public string IP { get; set; }
        public string Key { get; set; }
    }
    public class OBSConfig
    {
        public string Connectionstring { get; set; }
        public string Password { get; set; }
    }

    public class SpotifyConfig
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }

    public class BlacklightConfig
    {
        public string IP { get; set; }
    }
}

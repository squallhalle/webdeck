using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WebDeck.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebDeck.Models;
using System.Security.AccessControl;
using Newtonsoft.Json;
using OBS.WebSocket.NET;
using WebDeck.Models.SQL;
using WebDeck.Services;
using System.Net.Http;
using WebDeck.Hubs;

namespace WebDeck
{
    public class Startup
    {
        private readonly IWebHostEnvironment env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            this.env = env;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(options =>
                            options.UseSqlite(
                                Configuration.GetConnectionString("SQLLiteDefaultConnection")));
            services.AddDbContextPool<IconConfigContext>(options =>
                options.UseSqlite(
                    Configuration.GetConnectionString("SQLLiteIconConfigConnection")));
            ConfigModel configModel = ConfigModel.Load(env);
            services.AddSingleton<ConfigModel>(configModel);
            services.AddHttpClient("Blacklight", c =>
            {
                c.BaseAddress = new Uri($"http://{configModel.blacklightConfig.IP}");
            }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                ServerCertificateCustomValidationCallback =
            (httpRequestMessage, cert, cetChain, policyErrors) =>
            {
                return true;
            }
            });
            services.AddSingleton<HueService>();
            services.AddSingleton<OBSService>();
            services.AddSingleton<SpotifyService>();
            services.AddSingleton<MonsterCatService>();
            services.AddSingleton<ObsWebSocket>();
            services.AddSingleton<BlacklightService>();
            services.AddSignalR();
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddMvc();
            services.AddSingleton<DumbAssIconListForLazyDevs>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseMvcWithDefaultRoute();
            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapDefaultControllerRoute();
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapHub<DeckHub>("/DeckHub");
            });
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebDeck.Migrations.MonserDB
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Artist",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Public = table.Column<bool>(nullable: false),
                    Role = table.Column<string>(nullable: true),
                    Uri = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artist", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Release",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ArtistsTitle = table.Column<string>(nullable: true),
                    CatalogId = table.Column<string>(nullable: true),
                    ReleaseDate = table.Column<DateTimeOffset>(nullable: false),
                    Tags = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Upc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Release", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Results",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ArtistsTitle = table.Column<string>(nullable: true),
                    Bpm = table.Column<long>(nullable: false),
                    CreatorFriendly = table.Column<bool>(nullable: false),
                    DebutDate = table.Column<DateTimeOffset>(nullable: false),
                    Downloadable = table.Column<bool>(nullable: false),
                    Duration = table.Column<long>(nullable: false),
                    Explicit = table.Column<bool>(nullable: false),
                    GenrePrimary = table.Column<string>(nullable: true),
                    GenreSecondary = table.Column<string>(nullable: true),
                    InEarlyAccess = table.Column<bool>(nullable: false),
                    Isrc = table.Column<string>(nullable: true),
                    PlaylistSort = table.Column<long>(nullable: false),
                    Streamable = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    TrackNumber = table.Column<long>(nullable: false),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Results", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResultArtist",
                columns: table => new
                {
                    ResultId = table.Column<Guid>(nullable: false),
                    ArtistId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResultArtist", x => new { x.ResultId, x.ArtistId });
                    table.ForeignKey(
                        name: "FK_ResultArtist_Artist_ArtistId",
                        column: x => x.ArtistId,
                        principalTable: "Artist",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResultArtist_Results_ResultId",
                        column: x => x.ResultId,
                        principalTable: "Results",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ResultArtist_ArtistId",
                table: "ResultArtist",
                column: "ArtistId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Release");

            migrationBuilder.DropTable(
                name: "ResultArtist");

            migrationBuilder.DropTable(
                name: "Artist");

            migrationBuilder.DropTable(
                name: "Results");
        }
    }
}

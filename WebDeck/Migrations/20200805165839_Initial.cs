﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebDeck.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IconPage",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PageName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IconPage", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "IconConfig",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IconPath = table.Column<string>(maxLength: 50, nullable: false),
                    CommandType = table.Column<string>(maxLength: 50, nullable: false),
                    CommandText = table.Column<string>(maxLength: 50, nullable: false),
                    PosX = table.Column<int>(nullable: false),
                    PosY = table.Column<int>(nullable: false),
                    FK_Page = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "FK_TempSales_SalesReason",
                        column: x => x.FK_Page,
                        principalTable: "IconPage",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IconConfig_FK_Page",
                table: "IconConfig",
                column: "FK_Page");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IconConfig");

            migrationBuilder.DropTable(
                name: "IconPage");
        }
    }
}
